import $ from 'jquery';
class Modal {
    constructor() {
        this.openModalButtons = $('.open-modal');
        this.modal = $('.modal');
        this.modalCloseButton = $('.modal-close');
        this.events();
    }
    events() {
        this.openModalButtons.click(this.openModal.bind(this)); //this is used to bind this to this. Matlab abh uss function mei this will be button rather than event objct.
        this.modalCloseButton.click(()=>this.closeModal()); //this is same as above line.
        $(document).keyup(this.keyPressHandler.bind(this));

    }

    openModal() {
        this.modal.addClass('modal-is-visible');
        return false; //This will cause preventDefault() to be performed. Since false was returned from the callback function, it never performs the original task.
    }
    closeModal() {
        this.modal.removeClass('modal-is-visible');
    }
    keyPressHandler(e) {
        if(e.keyCode == 27) {
            this.closeModal();
        }
    }
}

export default Modal;