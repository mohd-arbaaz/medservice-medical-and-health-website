// 1. Select the element
// 2. Add Listener to it
// 3. Write a function which performs job when event occurs
// document.querySelector("selector").addEventListener("click", function(){
//
// })

class MobileMenu{
    constructor(){
        this.menuIcon = document.querySelector(".mobile-header-icon");
        this.mobileHeader = document.querySelector(".mobile-menu");
        this.events();
    }

    events() {
        this.menuIcon.addEventListener("click", ()=>this.toggleMenu()); //this shorthand prevent this from pointing something other than class. Agar mai this.toggleMenu() likhta toh this would be pointing to menuIcon element in html, and my code would not work. ()=> prevented the scope of 'this'.
    }
    toggleMenu(){
        this.mobileHeader.classList.toggle("mobile-menu-active");
        this.menuIcon.classList.toggle("mobile-header-icon-close");
    }

}

export default MobileMenu;